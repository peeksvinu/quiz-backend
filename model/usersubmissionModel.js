const mongoose = require("mongoose")
const usersubmissionSchema = new mongoose.Schema({
    questions: [
        {
            question: {
                type: mongoose.Types.ObjectId,
                ref: 'Question',
                required:true,
            },
            selectedOPT: {
                type:String, 
            }

        }]
});

const Useroption = mongoose.model('Useroption', usersubmissionSchema);
module.exports = Useroption;