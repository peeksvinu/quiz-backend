const mongoose = require("mongoose")
const subjectsSchema = new mongoose.Schema({
    sub: String
});

const Subject = mongoose.model('subject', subjectsSchema);
module.exports = Subject
