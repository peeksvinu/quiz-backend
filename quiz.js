const express = require("express");
const mongoose = require("mongoose")
var cors = require('cors')

const Question = require("./model/questionModel")
const Test = require("./model/testModel")
const Useroption = require("./model/usersubmissionModel")
const Subject = require("./model/subjectsModel")

const app = express();
const port = 3000;
app.use(express.json())
app.use(cors())


main().catch(err => console.log(err));
async function main() {
  await mongoose.connect('mongodb+srv://peeksvinu:Dh5zYoAWL8TrOZj4@cluster0.gwkdqeg.mongodb.net/?retryWrites=true&w=majority');
}


app.post("/subjects", async (req, res) => {
  const subjects = new Subject(req.body);
  await subjects.save()
  res.json(subjects).status(201)
});

app.get("/subjects", async (req, res) => {
  const subjects = await Subject.find({});
  res.json(subjects).status(200)
});



app.post("/tests", async (req, res) => {
  const test = new Test(req.body);
  await test.save()
  res.json(test).status(201)
});
app.get("/tests", async (req, res) => {
  const test = await Test.find({});
  res.json(test).status(200)
});



app.get("/questions", async (req, res) => {
  const questions = await Question.find({});
  res.json(questions).status(200)
});
app.post("/questions", async (req, res) => {
  const question = new Question(req.body);
  await question.save()
  res.json(question).status(201)
});



app.post("/useroption", async (req, res) => {
  const useroption = new Useroption(req.body);
  await useroption.save()
  res.json(useroption).status(201)
});



app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});